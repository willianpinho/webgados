//
//  AdListPresenter.swift
//  webgados
//
//  Created by Willian Pinho on 22/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation

protocol AdListPresenterView: NSObjectProtocol {
    func showAlert(title: String, message: String)
    func updateCells(ad: Ad)

}

class AdListPresenter: NSObject {
    
    var view : AdListPresenterView?
    
    func setViewDelegate(view: AdListPresenterView) {
        self.view = view
    }
    
    func loadAdsFromServer(completion: @escaping (Bool?, [Ad]?, String?) -> Void) {
        AdService.getAllAds { (success, ads, message) in
            if success! {
                completion(true, ads, "Tudo ok")
            } else {
                completion(false, nil, "Erro")

            }
        }
    }
}
