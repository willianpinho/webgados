//
//  AdListViewController.swift
//  webgados
//
//  Created by Willian Pinho on 22/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import UIKit
import PINRemoteImage
import PINCache

class AdListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var presenter = AdListPresenter()
    var ads: [Ad] = []
    var cells: [UITableViewCell] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPresenterDelegate()
        self.configureTableView(tableView: self.tableView)
        self.loadAds()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
    }
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    func loadAds(){
        self.presenter.loadAdsFromServer { (success, ads, message) in
            if success! {
                self.ads = ads!
                self.populateTableview(ads: self.ads)
            } else {
                self.showAlert(title: "Ops", message: message!)
            }
        }
    }
    
    func configureTableView(tableView: UITableView) {
        self.tableView.register(UINib(nibName: "AdTableViewCell", bundle: nil), forCellReuseIdentifier: "AdTableViewCell")
        
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func populateTableview(ads: [Ad]) {
        for a in ads {
            self.updateCells(ad: a)
        }
        
        self.tableView.reloadData()
    }
    
}

extension AdListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.cells[indexPath.row]
    }
    
}

extension AdListViewController: AdListPresenterView {
    /**
     Create AdTableViewCell to tableView
     
     - Parameters:
     - Ad
     
     - Returns:
     - AdTableViewCell
     */
    
    func updateCells(ad: Ad) {
        if let adtvc = self.tableView.dequeueReusableCell(withIdentifier: "AdTableViewCell") as? AdTableViewCell {
            if let image = ad.photos.first {
                adtvc.imageLabel.pin_updateWithProgress = true
                let urlImage = URL(string: image)
                adtvc.imageLabel?.pin_setImage(from: urlImage)
            }
            
            if let title = ad.title {
                adtvc.titleLabel.text = title
            }
            
            if let age = ad.age {
                adtvc.ageLabel.text = age
            }
            
            if let weight = ad.weight.value, let type = ad.weightType {
                adtvc.weightLabel.text = "\(weight) \(type)"
            }
            
            if let value = ad.value {
                adtvc.valueLabel.text = value
            }
            
            if let city = ad.farm?.city?.name, let state = ad.farm?.city?.state {
                adtvc.cityLabel.text = "\(city) - \(state) \na 30 km de você"
            }
            
            if let sellerName = ad.seller?.name {
                adtvc.sellerNameLabel.text = sellerName
            }
            
            if let sellerAvatar = ad.seller?.urlImage {
                adtvc.imageLabel.pin_updateWithProgress = true
                let urlImage = URL(string: sellerAvatar)
                adtvc.sellerAvatarImageView?.pin_setImage(from: urlImage)
            }
                        
            cells.append(adtvc)
        }
        
        
    }
    
    /**
     Create Alert and show to user
     
     - Parameters:
     - title
     - message
     
     - Returns:
     - Present alert to User
     */
    func showAlert(title: String, message: String) {
        let alert = Alert.showMessage(title: title, message: message)
        self.present(alert, animated: true, completion: nil)
    }
}


