//
//  AdTableViewCell.swift
//  webgados
//
//  Created by Willian Pinho on 22/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import UIKit

class AdTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageLabel: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!

    @IBOutlet weak var sellerNameLabel: UILabel!
    @IBOutlet weak var sellerAvatarImageView: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.sellerAvatarImageView.layer.cornerRadius = self.sellerAvatarImageView.frame.size.width / 2
        self.sellerAvatarImageView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
