//
//  Ad.swift
//  webgados
//
//  Created by Willian Pinho on 22/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import RealmSwift

class Ad: Object {
    @objc dynamic var id: String?
    @objc dynamic var title: String?
    @objc dynamic var age: String?
    var weight = RealmOptional<Int>()
    @objc dynamic var weightType: String?
    @objc dynamic var value: String?
    
    @objc dynamic var seller: Seller?
    @objc dynamic var farm: Farm?
    var photos = List<String>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
