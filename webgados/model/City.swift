//
//  City.swift
//  webgados
//
//  Created by Willian Pinho on 22/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import RealmSwift

class City: Object {
    @objc dynamic var id: String?
    @objc dynamic var name: String?
    @objc dynamic var state: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
