//
//  Seller.swift
//  webgados
//
//  Created by Willian Pinho on 22/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import RealmSwift

class Seller: Object {
    @objc dynamic var id: String?
    @objc dynamic var name: String?
    @objc dynamic var urlImage: String?
    @objc dynamic var type: String?
    @objc dynamic var celular: String?

    let rating = RealmOptional<Double>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
