//
//  AdService.swift
//  webgados
//
//  Created by Willian Pinho on 22/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class AdService {
    static func getAllAds(completion: @escaping (Bool?, _ ads: [Ad]?, _ message: String?) -> Void) {
        let url = URL(string: "https://webgados.com.br/anuncios-example")
        let parameters = Parameters()
        
        Alamofire.request(url!, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseSwiftyJSON { dataResponse in
                if dataResponse.result.isSuccess {
                    let ads = parseAds(result: dataResponse.value!)
                    completion(true,ads,"Sucesso")

                } else {
                    completion(false,nil,"erro")

                }
        }
        
    }
    
    private static func parseAds(result: JSON) -> [Ad] {
        var ads: [Ad] = []
        let arrayAds = result["ads"].arrayValue
        for a in arrayAds {
            let ad = Ad()
            ad.id = a["id"].stringValue
            ad.title = a["title"].stringValue
            if let years = a["years"].int {
                ad.age = "\(years) ano(s)"
            }
            
            if let months = a["months"].int {
                if ad.age != nil {
                    ad.age = ad.age! + " e \(months) mes(es)"
                } else {
                    ad.age = "\(months) meses"
                }
            }
            
            if ad.age == nil {
                ad.age = "Sem idade"
            }
            
            if let weight = a["average_weight"].int {
                ad.weight.value = weight
            }
            
            if let weightType = a["weight_type"].string {
                if weightType == "@" {
                    ad.weightType = "arroba(s)"
                }
            }
            
            if let value = a["unit_value"].int {
                ad.value = "R$ \(value/100),00"
            }
            
            let seller = Seller()
            if let sellerId = a["seller_id"].int {
                seller.id = String(sellerId)
            }
            if let sellerName = a["seller_name"].string {
                seller.name = sellerName
            }
            
            if let sellerAvatar = a["seller_avatar"].string {
                seller.urlImage = sellerAvatar
            }
            
            seller.rating.value = a["seller_rating"].doubleValue
            ad.seller = seller
            
            if let photos = a["photos_url"].array {
                for p in photos {
                    ad.photos.append(p.string!)
                }
            }
            
            let farm = Farm()
            farm.street = a["farm"]["street"].stringValue
            
            let city = City()
            city.id = a["farm"]["city_id"].stringValue
            city.name = a["farm"]["city"].stringValue
            city.state = a["farm"]["state"].stringValue
            
            farm.city = city
            ad.farm = farm

            ads.append(ad)
        }
        
        return ads
    }
}
