//
//  Alert.swift
//  webgados
//
//  Created by Willian Pinho on 22/11/17.
//  Copyright © 2017 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

class Alert {
    static func showMessage(title: String?, message: String?) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Ok", style: .default) { (alertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okButton)
        return alert
    }
}

